#include <stdio.h>
#include "led.h"
#include "uart.h"

int main(void)
{
	LedOutputCfg();
	UartConfig();
	
	while (1)
	{
		LedOn();
		UartPuts("led on\n\r");
		//printf("Led on\n\r");
		for (int i=0; i<1000000; i++);
		LedOff();
		UartPuts("led off\n\r");
		//printf("Led off\n\r");
		for (int i=0; i<1000000; i++);
	}
}
